var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost/Web_Course';
var Emitter = require('events').EventEmitter;
var  findUtils = require("FindUtils");//PTR TO NEW MODULE THAT HANDLES THE Finds
var InsertUtils = require("InsertUtils");//PTR TO NEW MODULE THAT HANDLES THE INSERTIONS
var UpdateUtils = require("UpdateUtils");//PTR TO NEW MODULE THAT HANDLES THE UPDATES
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//the get param we get is type of the requset whether show or insert a user or check if exist
//we insert user,show users,and when login check if exist
//req.query use to get 'get' params
app.get('/Users/', function (req, res) 
{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
   MongoClient.connect(url, function(err, db) 
   {
	  assert.equal(null, err);
	  var type = req.query.type;
	  console.log("the type of the request for Users is : " + type);
	  if(type === "show")
  	  {
  
		  findUtils . findAllUser(db, function(output) 
		  {
		   	console.log("completed");
		   	db.close();
	  	   	res.send(output);	
	  	   	res.end();
	  	   });
	  }
	  else if(type === "insert")
	  {
		var firstName = req.query.firstName;
		var lastName  =  req.query.lastName;
		var email   =   req.query.email;
		var gender =    req.query.gender;
		var phone  =    req.query.phone;
		var theDate =Date();
		var newUser=
		{	
        		"FirstName" : firstName,
        		"LastName" : lastName,
        		"gender" : gender,
        		"PhoneNumber" : phone,
        		"Email" : email,
			"CreateTime" : theDate
		}
		InsertUtils . InsertUser(db,newUser, function(output)
                {
                        db.close();
			console.log("the callback from function is :" + output);
                        res.send(output);
                        res.end();
                });
	
	  }
	  else if(type === "exist")
	  {
		var firstName = req.query.firstName;
		var password  = req.query.password;
		findUtils . isExist(db,firstName,password, function(output)
		{
                        console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
                });	
	  }
	  else
	  {
		res.redirect('http://52.24.95.249/Hotels/Error.html');
		res.end();
	  }
 	  
    });

})

app.get('/Users/Check', function (req, res) 
{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
   MongoClient.connect(url, function(err, db) 
   {
		var email  = req.query.email;
		findUtils . isExistUserForRemove(db,email, function(output)
		{
                        console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
                });	
	  
	  
 	  
    });

})

//we use post for comments in case of adding comment beacuse in get requset we may exceed the url length 
//req.body use for 'post' params
app.post('/Hotels/Comments',function(req, res)
{
   //THE 2 follows res.header is for allow accesing the Hotels/Comments from different html page
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  MongoClient.connect(url, function(err, db)
  {

	  assert.equal(null, err);
          var type = req.body.type;
          console.log("the type of the request for Users is : " + type);
	  if(type === "commentAdd")
          {
                var address = req.body.address;
		var userAvg = req.body.avg;
		var email  = req.body.email;
		var cat1A = req.body.cat1A;
                var cat1B = req.body.cat1B;
                var cat1C = req.body.cat1C;
                var cat2A = req.body.cat2A;
                var cat2B = req.body.cat2B;
                var cat2C = req.body.cat2C;
                var cat3A = req.body.cat3A;
                var cat3B = req.body.cat3B;
                var cat3C = req.body.cat3C;
                var cat3D = req.body.cat3D;
		var review =
{
	  Email:email,	 
      QuestionStock:
	  [
		{
			OverollExperience:
			[
				{
					Question:"Please rate your overall satisfaction with your most recent stay at this hotel",
					Qid:"cat1A",
					Response:Number(cat1A)
				},
				{
					Question:"How likely would you be to recommend this hotel to a friend planning to visit the area  ?",
					Qid:"cat1B",
					Response:Number(cat1B)
				},
				{
					Question:"Thinking about your overall experience at this hotel and what you paid, how would you rate the value you received for the money? Was it worth it?",
					Qid:"cat1C",
					Response:Number(cat1C)
				}
			],
			OverollImpression:
			[
				{
					Question:"Overall service received",
					Qid:"cat2A",
					Response:Number(cat2A)
				},
				{
					Question:"Appearance of hotel exterior",
					Qid:"cat2B",
					Response:Number(cat2B)
				},
				{
					Question:"Condition of lobby and reception area",
					Qid:"cat2C",
					Response:Number(cat2C)
				}
			],
			AboutGuestRoom:
			[
				{
					Question:"Overall cleanliness of guest room",
					Qid:"cat3A",
					Response:Number(cat3A)
				},
				{
					Question:"Comfort of mattress",
					Qid:"cat3B",
					Response:Number(cat3B)
				},
				{
					Question:"Comfort of pillows",
					Qid:"cat3C",
					Response:Number(cat3C)
				},
				{
					Question:"Ease of using Internet service",
					Qid:"cat3D",
					Response:Number(cat3D)
				}
			]
  
		}
	  
	  ]
	  
    }
		findUtils . findHotelAvg(db,address,function(output)
                 {
				var updateAvg;
                                        console.log("avg from hotels address " +address +"is" +output );
                                        var avgTotal = Number((Number(output) +Number(userAvg))/2);
                                        console.log("avg new total  is "+avgTotal);

                                        updateAvg = avgTotal;
                                         console.log("the updated avg "+updateAvg);


			UpdateUtils . AddComment(db,address,updateAvg,review,function(output)
                	{
                	        console.log("completed");
                       		 db.close();
                       		 res.send(output);
                       		 res.end();
                	});
		});











	  }
	

	













  });

})
//handle show hotels and show hotel comment ,  ,remove comment
app.get('/Hotels', function (req, res)
{
  //THE 2 follows res.header is for allow accesing the Hotels/ from different html page
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
   MongoClient.connect(url, function(err, db)
   {
          assert.equal(null, err);
          var type = req.query.type;
          console.log("the type of the request for Users is : " + type);
	if(type === "show")
	{
		findUtils . getHotels(db,function(output)
                {
                        console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
                });

	}
	else if(type === "commentRemove")
	{
		var address = req.query.address;
		var email   = req.query.email;
		 UpdateUtils . RemoveComment(db,address,email,function(output)
                {

                        console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
                });

	}
	else if(type === "UserRemove")
	{
		var email   = req.query.email;
		 UpdateUtils . RemoveUser(db,email,function(output)
                {

                        console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
                });

	}
	 else if(type === "commentShowAll")
        {
                var address = req.query.address;
                 findUtils . findHotelReviews(db,address,function(output)
                {

                        console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
                });

        }
	else if(type ==="getBest")
	{
	   findUtils . findBestHotels(db,function(output)
	   {
			console.log("completed");
                        db.close();
                        res.send(output);
                        res.end();
	   });
	}

	else
          {
                res.redirect('http://52.24.95.249/Hotels/Error.html');
                res.end();
          }
   });

})


app.get('/HotelIt', function (req, res)
{
       	//Welcome.html is our first page 
	 res.redirect('http://52.24.95.249/Hotels/Login.html');
         res.end();
})

function matan_test(msg)
{
  console.log("in matan_test GO EVENT"+msg);
}

app.get('/Emitter', function (req, res)
{
	var log = new Emitter();
	log.on('go',matan_test);
	log.emit('go',"server start for matan");
	 log.emit('go',"bye bye");
	 res.end();
})


var server = app.listen(1027, function () {

  var host = "localhost";
  var port = server.address().port;

  console.log("Example app listening at http://%s:%s", host, port);

})
