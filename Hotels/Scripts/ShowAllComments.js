$(document).ready(function()
{
     $('body').on('click', '#ReviewsInfo', function ()
      {
        getReviews();
      }
     );
    function getReviews()
       {

                var _type="commentShowAll";
		var address =$('#address').val();

                $.get('http://52.24.95.249:1027/Hotels', {
                        type : _type,
                        address : address

                }
                 , function(responseText)
                {
					//Handle Hotel Name
					//======================
					$('#HotelName').html("<h1><u>" + responseText[0].Name + "</u></h1><br><br>");
					//===============Handle Cat  1 questions
					var cat1 = "<h2><u>Overoll Experience </u></h1> \n <TABLE BORDER=1 class=\"table table-striped\" ALIGN=CENTER>\n" +
                    "<TR BGCOLOR=\"#FFAD00\">\n" + 
					"<TH>Please rate your overall satisfaction with your most recent stay at this hotel\n" 
					+"<TH>How likely would you be to recommend this hotel to a friend planning to visit the area  ?\n"
					+"<TH>Thinking about your overall experience at this hotel and what you paid, how would you rate the value you received for the money? Was it worth it?\n"
					+"<TH> Email \n" 
					+ "<TR> \n";
						
						 //LOOP OVER THE REVIEWS
						 for(var j = 0 ; j <  responseText[0].Reviews.length ; j++)
						 {
							//get first categrory
							 for (var k = 0 ; k < responseText[0].Reviews[j].QuestionStock[0].OverollExperience.length ; k++)
							 {
								 cat1 +="<TD>" + responseText[0].Reviews[j].QuestionStock[0].OverollExperience[k].Response + "\n";
								 
							 }
							 //enter the mail
							 cat1 +="<TD>" + responseText[0].Reviews[j].Email+"\n";
							 cat1 +="<TR> \n";
							 
						   
						 }
						 //end of cat1
						 cat1 +="</TABLE>\n<br><br>";
						 //===============Handle Cat  2 questions
						 var cat2 = "<h2><u>Overoll Impression </u></h1> \n <TABLE BORDER=1 class=\"table table-striped\" ALIGN=CENTER>\n" +
                    "<TR BGCOLOR=\"#FFAD00\">\n" + 
					"<TH>Overall service received\n" 
					+"<TH>Appearance of hotel exterior  ?\n"
					+"<TH>Condition of lobby and reception area?\n"
					+"<TH> Email \n" 
					+ "<TR> \n";
						
						 //LOOP OVER THE REVIEWS
						 for(var j = 0 ; j <  responseText[0].Reviews.length ; j++)
						 {
							//get second categrory
							 for (var k = 0 ; k < responseText[0].Reviews[j].QuestionStock[0].OverollImpression.length ; k++)
							 {
								 cat2 +="<TD>" + responseText[0].Reviews[j].QuestionStock[0].OverollImpression[k].Response + "\n";
								 
							 }
							 //enter the mail
							 cat2 +="<TD>" + responseText[0].Reviews[j].Email+"\n";
							 cat2 +="<TR> \n";
							 
						   
						 }
						   //end of cat2	  
						 cat2 +="</TABLE>\n";
						 //===============Handle Cat  3 questions
					var cat3 = "<h2><u>About Guest Room </u></h1> \n <TABLE BORDER=1 class=\"table table-striped\" ALIGN=CENTER>\n" +
                    "<TR BGCOLOR=\"#FFAD00\">\n" + 
					"<TH>Overall cleanliness of guest room" 
					+"<TH>Comfort of mattress  ?\n"
					+"<TH>Comfort of pillows?\n"
					+"<TH>Ease of using Internet service\n"
					+"<TH> Email \n" 
					+ "<TR> \n";
						
						 //LOOP OVER THE REVIEWS
						 for(var j = 0 ; j <  responseText[0].Reviews.length ; j++)
						 {
							//get third categrory
							 for (var k = 0 ; k < responseText[0].Reviews[j].QuestionStock[0].AboutGuestRoom.length ; k++)
							 {
								 cat3 +="<TD>" + responseText[0].Reviews[j].QuestionStock[0].AboutGuestRoom[k].Response + "\n";
								 
							 }
							 //enter the mail
							 cat3 +="<TD>" + responseText[0].Reviews[j].Email+"\n";
							 cat3 +="<TR> \n";
							 
						   
						 }
						 var sumCat = cat1 + cat2 + cat3 ;
						$('#ReviewsTable').html(sumCat);
					 

                });


        }




});//end of document ready

